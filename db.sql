--CREATE USER 'user_dar'@'localhost' IDENTIFIED BY '123456';
--GRANT ALL PRIVILEGES ON * . * TO 'user_dar'@'localhost';
--FLUSH PRIVILEGES;

DROP DATABASE dar_db;
CREATE DATABASE dar_db;

USE dar_db;

DROP TABLE IF EXISTS lines_tab;
CREATE TABLE lines_tab (
	id_line VARCHAR(30) NOT NULL PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	code VARCHAR(10) NOT NULL ,
	type VARCHAR(10) NOT NULL
);

DROP TABLE IF EXISTS stop_areas_tab;
CREATE TABLE stop_areas_tab (
	id_area VARCHAR(30) NOT NULL PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	lat VARCHAR(15) NOT NULL ,
	lon VARCHAR(15) NOT NULL, 
	maj BOOL NOT NULL
);

DROP TABLE IF EXISTS prochains_departs_tab;
CREATE TABLE prochains_departs_tab (
        id INTEGER NOT NULL PRIMARY KEY,   
	id_area VARCHAR(30) NOT NULL,
	id_line VARCHAR(30) NOT NULL,
	direction VARCHAR(100) NOT NULL,
	time_depart DATETIME NOT NULL
);
