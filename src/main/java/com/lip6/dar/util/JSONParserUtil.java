package com.lip6.dar.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.lip6.dar.hibernate.LinesTab;
import com.lip6.dar.hibernate.ProchainsDepartsTab;
import com.lip6.dar.hibernate.StopAreasTab;

public class JSONParserUtil {

	private static final Log log = LogFactory.getLog(JSONParserUtil.class);

	// parse all information of line as idLine name, code, type
	public static List<LinesTab> parseLine(String str, String type) {
		log.info("begin parseLine");
		List<LinesTab> lines = new ArrayList<>();
		JSONParser jsonParser = new JSONParser();
		try {
			JSONObject jsonObject = (JSONObject) jsonParser.parse(str);
			// get array of line
			JSONArray lineArr = (JSONArray) jsonObject.get("lines");
			if (lineArr != null && lineArr.size() > 0) {
				Iterator line = lineArr.iterator();
				while (line.hasNext()) {
					JSONObject innerObj = (JSONObject) line.next();
					LinesTab lineTab = new LinesTab();
					lineTab.setIdLine((String) innerObj.get("id"));
					lineTab.setName((String) innerObj.get("name"));
					lineTab.setCode((String) innerObj.get("code"));
					lineTab.setType(type);
					lines.add(lineTab);
				}
			} else {
				log.info("........ There is no line...........");
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.debug(e);
		}
		log.info("begin parseLine");
		return lines;
	}

	// parse all information of line as idArea, name, lat, lon
	public static List<StopAreasTab> parseStopAreas(String str, String type) {
		log.info("begin parseStopAreas");
		List<StopAreasTab> stop_areas = new ArrayList<>();
		JSONParser jsonParser = new JSONParser();
		try {
			JSONObject jsonObject = (JSONObject) jsonParser.parse(str);
			// get array of stop_areas
			JSONArray stopArr = (JSONArray) jsonObject.get("stop_areas");
			if (stopArr != null && stopArr.size() > 0) {
				Iterator stop = stopArr.iterator();
				while (stop.hasNext()) {
					JSONObject innerObj = (JSONObject) stop.next();
					StopAreasTab stopTab = new StopAreasTab();
					stopTab.setIdArea((String) innerObj.get("id"));
					stopTab.setName((String) innerObj.get("name"));
					JSONObject coordObj = (JSONObject) innerObj.get("coord");					
					stopTab.setLat((String) coordObj.get("lat"));
					stopTab.setLon((String) coordObj.get("lon"));
					stopTab.setMaj(false);
					stop_areas.add(stopTab);
				}
			} else {
				log.info("........ There is no stop areas...........");
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			log.debug(e);
		}
		log.info("end parseStopAreas");
		return stop_areas;
	}

	// parse all information of prochain depart as idArea, idLine, direction, timeDepart
		public static List<ProchainsDepartsTab> parseNextTrain(String str) throws java.text.ParseException {
			log.info("begin parseNextTrain");
			List<ProchainsDepartsTab> nextTrain = new ArrayList<>();
			JSONParser jsonParser = new JSONParser();
			try {
				JSONObject jsonObject = (JSONObject) jsonParser.parse(str);
				// get array of departures
				JSONArray departArr = (JSONArray) jsonObject.get("departures");
				if (departArr != null && departArr.size() > 0) {
					Iterator depart = departArr.iterator();
					while (depart.hasNext()) {
						JSONObject innerObj = (JSONObject) depart.next();
						ProchainsDepartsTab departTab = new ProchainsDepartsTab();
						JSONObject line = (JSONObject) ((JSONObject) innerObj.get("route")).get("line");
						departTab.setIdLine((String) line.get("id"));						
						departTab.setIdArea((String) (((JSONObject)((JSONObject)innerObj.get("stop_point")).get("stop_area"))).get("id"));						
						departTab.setDirection((String) (((JSONObject) innerObj.get("route")).get("name")));
						String datetime = (String) (((JSONObject) innerObj.get("stop_date_time")).get("departure_date_time"));
						datetime = datetime.replaceFirst("T", "");						
						departTab.setTimeDepart(DateUtil.convertStringToDate("yyyyMMddHHmmss", datetime));
						nextTrain.add(departTab);
					}
				} else {
					log.info("........ There is no next train for stop areas...........");
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				log.debug(e);
			}
			log.info("end parseStopAreas");
			return nextTrain;
		}
}
