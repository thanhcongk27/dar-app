package com.lip6.dar.util;

import java.io.IOException;
import java.util.Properties;

public class NavitiaProperties {
	private static Properties props;

	  private NavitiaProperties() {
	    props = new Properties();
	    try {
	      props.load(getClass().getResourceAsStream("/config.properties"));
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }

	  /**
	   * Get a hold of the instance to access properties.
	   * @return
	   */
	  public static NavitiaProperties getInstance() {
	    return Holder.instance;
	  }
	  private static class Holder {
	    static NavitiaProperties instance = new NavitiaProperties();
	  }

	  public String getProperty(String key) {
	    return props.getProperty(key);
	  }
	  public String getProperty(String key, String defaultValue) {
	    return props.getProperty(key, defaultValue);
	  }
}
