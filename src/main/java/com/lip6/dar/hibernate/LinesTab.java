package com.lip6.dar.hibernate;


// Generated Oct 19, 2014 1:28:30 AM by Hibernate Tools 3.4.0.CR1

/**
 * LinesTab generated by hbm2java
 */

public class LinesTab implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1799030165435926085L;

	private String idLine;
	private String name;
	private String code;
	private String type;

	public LinesTab() {
	}

	public LinesTab(String idLine, String name, String code, String type) {
		this.idLine = idLine;
		this.name = name;
		this.code = code;
		this.type = type;
	}

	public String getIdLine() {
		return this.idLine;
	}

	public void setIdLine(String idLine) {
		this.idLine = idLine;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
