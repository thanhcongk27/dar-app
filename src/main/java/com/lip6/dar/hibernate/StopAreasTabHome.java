package com.lip6.dar.hibernate;

// Generated Oct 19, 2014 1:28:30 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

import com.lip6.dar.util.HibernateUtil;

/**
 * Home object for domain model class StopAreasTab.
 * @see com.lip6.dar.hibernate.StopAreasTab
 * @author Hibernate Tools
 */
public class StopAreasTabHome {

	private static final Log log = LogFactory.getLog(StopAreasTabHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public void persist(StopAreasTab transientInstance) {
		log.debug("persisting StopAreasTab instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(StopAreasTab instance) {
		log.debug("attaching dirty StopAreasTab instance");
		try {
			Transaction trans=sessionFactory.getCurrentSession().beginTransaction(); 
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			trans.commit();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(StopAreasTab instance) {
		log.debug("attaching clean StopAreasTab instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(StopAreasTab persistentInstance) {
		log.debug("deleting StopAreasTab instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public StopAreasTab merge(StopAreasTab detachedInstance) {
		log.debug("merging StopAreasTab instance");
		try {
			StopAreasTab result = (StopAreasTab) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public StopAreasTab findById(java.lang.String id) {
		log.debug("getting StopAreasTab instance with id: " + id);
		try {
			StopAreasTab instance = (StopAreasTab) sessionFactory
					.getCurrentSession().get(
							"com.lip6.dar.hibernate.StopAreasTab", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(StopAreasTab instance) {
		log.debug("finding StopAreasTab instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.lip6.dar.hibernate.StopAreasTab")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List getAllIdArea() {
		log.debug("getAllIdArea");
		try {
			Transaction trans = sessionFactory.getCurrentSession().beginTransaction(); 
			List results = sessionFactory.getCurrentSession().createQuery("from StopAreasTab as stop where stop.maj = false").list();
			trans.commit();		
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public int updateMAJForAll (boolean maj) {
						
		log.debug("updateMAJForAll");
		try {
			Transaction trans = sessionFactory.getCurrentSession().beginTransaction(); 
			Query query = sessionFactory.getCurrentSession().createQuery("update StopAreasTab set maj = :maj");
			query.setParameter("maj", maj);	
			int result = query.executeUpdate();
			trans.commit();		
			log.debug("updateMAJForAll successful, result size: " + result);
			return result;
		} catch (RuntimeException re) {
			log.error("updateMAJForAll failed", re);
			throw re;
		}
	}	
}
