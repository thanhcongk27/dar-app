package com.lip6.dar.hibernate;

// Generated Oct 19, 2014 1:28:30 AM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

import com.lip6.dar.util.HibernateUtil;

/**
 * Home object for domain model class LinesTab.
 * @see com.lip6.dar.hibernate.LinesTab
 * @author Hibernate Tools
 */
public class LinesTabHome {

	private static final Log log = LogFactory.getLog(LinesTabHome.class);

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	

	public void persist(LinesTab transientInstance) {
		log.debug("persisting LinesTab instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(LinesTab instance) {
		log.debug("attaching dirty LinesTab instance");
		try {
			Transaction trans=sessionFactory.getCurrentSession().beginTransaction(); 
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			trans.commit();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(LinesTab instance) {
		log.debug("attaching clean LinesTab instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(LinesTab persistentInstance) {
		log.debug("deleting LinesTab instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public LinesTab merge(LinesTab detachedInstance) {
		log.debug("merging LinesTab instance");
		try {
			LinesTab result = (LinesTab) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public LinesTab findById(java.lang.String id) {
		log.debug("getting LinesTab instance with id: " + id);
		try {
			LinesTab instance = (LinesTab) sessionFactory.getCurrentSession()
					.get("com.lip6.dar.hibernate.LinesTab", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(LinesTab instance) {
		log.debug("finding LinesTab instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("com.lip6.dar.hibernate.LinesTab")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
