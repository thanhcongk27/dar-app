package com.lip6.dar.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;

import com.lip6.dar.util.NavitiaProperties;

public class NavitiaAPI {
	private static final Log log = LogFactory.getLog(NavitiaAPI.class);

	private String login;
	private String password;
	private NavitiaProperties prop = NavitiaProperties.getInstance();
	private String api_url = "https://api.navitia.io/v1/";

	public NavitiaAPI() {
		this.login = prop.getProperty("navitia.login");
		this.password = prop.getProperty("navitia.password");
	}

	private String doGet(String url) {
		log.debug("begin doGet");
		String result = null;
		try {
			HttpClient httpclient = new DefaultHttpClient();
			// Prepare a request object
			HttpGet httpget = new HttpGet(this.api_url + url);
			// Accept JSON
			httpget.addHeader("accept", "application/json");
			// Add authentication
			httpget.addHeader(BasicScheme.authenticate(
					new UsernamePasswordCredentials(this.login, this.password),
					"UTF-8", false));
			// Execute the request
			HttpResponse response = httpclient.execute(httpget);
			// Get the response entity
			HttpEntity entity = response.getEntity();
			// If response entity is not null
			if (entity != null) {
				// get entity contents and convert it to string
				InputStream instream = entity.getContent();
				result = getStringFromInputStream(instream);
				// Closing the input stream will trigger connection release
				instream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return the json
		log.debug("end doGet");
		return result;
	}

	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	// get all line by mode Train, Metro, Tramway
	public String getLinesByMode(String mode) {
		return doGet("coverage/fr-idf/physical_modes/physical_mode:" + mode + "/lines?count=1000");
	}
	
	// get all stop_areas by mode Train, Metro, Tramway
	public String getStopAreasByMode(String mode) {
		return doGet("coverage/fr-idf/physical_modes/physical_mode:" + mode + "/stop_areas?count=1000");
	}
	
	// get prochaine depart a partir un stop_area
	public String getNextTrain(String stop_area_id, String datetime) {
		return doGet("coverage/fr-idf/stop_areas/" + stop_area_id + "/departures?from_datetime=" + datetime);
	}
}
