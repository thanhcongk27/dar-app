package com.lip6.dar;

import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lip6.dar.api.NavitiaAPI;
import com.lip6.dar.hibernate.ProchainsDepartsTab;
import com.lip6.dar.hibernate.ProchainsDepartsTabHome;
import com.lip6.dar.hibernate.StopAreasTab;
import com.lip6.dar.hibernate.StopAreasTabHome;
import com.lip6.dar.util.DateUtil;
import com.lip6.dar.util.JSONParserUtil;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final Log log = LogFactory.getLog(App.class);
    public static void main( String[] args )
    {       
        getAllNextTrain();
    }
    
    private static void getAllNextTrain() {
    	log.info("begin getAllNextTrain");
    	StopAreasTabHome stopDAO = new StopAreasTabHome();
    	ProchainsDepartsTabHome departDAO = new ProchainsDepartsTabHome();
    	boolean isCompleted = false;
    	while (!isCompleted) {
        	//get all stop areas
        	List<StopAreasTab> areasList = stopDAO.getAllIdArea();
        	if (null != areasList && areasList.size() > 0) {
        		NavitiaAPI api = new NavitiaAPI();
        		String datetime = DateUtil.getDateTime("yyyyMMddHHmm", new Date());
        		// insert T between Date and hour like format on Navitia YYYYMMDDTHHmm
        		datetime = new StringBuilder(datetime).insert(8, "T").toString();        		
        		StopAreasTab area = null;
        		for (Iterator iterator = areasList.iterator(); iterator.hasNext();) {
        			area = (StopAreasTab) iterator.next();
    				String nextTrains = api.getNextTrain(area.getIdArea(), datetime);
    				try {
    					List<ProchainsDepartsTab> departList = JSONParserUtil.parseNextTrain(nextTrains);
    					if (null != departList && departList.size() > 0) {
    						for (ProchainsDepartsTab departTab : departList) {
    							departDAO.attachDirty(departTab);
    						}    						
    					} else {
    						log.debug("there is no next train of : " + area.getIdArea());
    					}
    					// update column maj = true
		        		area.setMaj(true);
		        		stopDAO.attachDirty(area);
    					
    				} catch (ParseException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}    				    			
    			}        		
        	} else {
        		// update all column maj = false
        		stopDAO.updateMAJForAll(false);
        		isCompleted = true;
        	}
		}    	    	   
    	log.info("end getAllNextTrain");
    }
}
