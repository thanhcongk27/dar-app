package com.lip6.dar;

import java.text.ParseException;
import java.util.Date;

import com.lip6.dar.util.DateUtil;

import junit.framework.TestCase;

public class DateUtilTest extends TestCase {
	
	public void testGetDateTime() {
		System.out.println(new Date());
		String datetime = DateUtil.getDateTime("yyyyMMddHHmm", new Date());
		System.out.println(datetime);
	}
	
	public void testConvertStringToDate() throws ParseException {
		Date datetime = DateUtil.convertStringToDate("yyyyMMddHHmm", "201410281635");
		System.out.println(datetime);
	}
}
