package com.lip6.dar;

import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lip6.dar.api.NavitiaAPI;
import com.lip6.dar.hibernate.LinesTab;
import com.lip6.dar.hibernate.LinesTabHome;
import com.lip6.dar.hibernate.ProchainsDepartsTab;
import com.lip6.dar.hibernate.ProchainsDepartsTabHome;
import com.lip6.dar.hibernate.StopAreasTab;
import com.lip6.dar.hibernate.StopAreasTabHome;
import com.lip6.dar.util.EnumTransit;
import com.lip6.dar.util.HibernateUtil;
import com.lip6.dar.util.JSONParserUtil;

public class NavitiaAPITest extends TestCase {

	private static final Log log = LogFactory.getLog(LinesTabHome.class);





	public void testGetAreas() {
		
		NavitiaAPI api = new NavitiaAPI();
		for (EnumTransit mode : EnumTransit.values()) {
			
			String json_stop_areas = api.getStopAreasByMode(mode.name());
			List<StopAreasTab> lines = JSONParserUtil.parseStopAreas(json_stop_areas, mode.name());
			// insert to database
			if (null != lines & lines.size() > 0) {
				StopAreasTabHome stopDAO = new StopAreasTabHome();
				Iterator<StopAreasTab> i = lines.iterator();
				while (i.hasNext()) {
					StopAreasTab stop = (StopAreasTab) i.next();
					stopDAO.attachDirty(stop);
				}
			}
			
			
		}		
		HibernateUtil.shutdown();
	}
	
	public void testGetLines() {
		
		NavitiaAPI api = new NavitiaAPI();
		for (EnumTransit mode : EnumTransit.values()) {
			
			String json_lines = api.getLinesByMode(mode.name());
			List<LinesTab> lines = JSONParserUtil.parseLine(json_lines, mode.name());
			// insert to database
			if (null != lines & lines.size() > 0) {
				LinesTabHome lineDAO = new LinesTabHome();
				Iterator<LinesTab> i = lines.iterator();
				while (i.hasNext()) {
					LinesTab line = (LinesTab) i.next();
					lineDAO.attachDirty(line);
				}
			}
			
			
		}		
		HibernateUtil.shutdown();
	}
	
	public void testProchain() throws ParseException {
			
			NavitiaAPI api = new NavitiaAPI();
			String nextTrains = api.getNextTrain("stop_area:RTP:SA:1907", "20141102T1029");
			
			try {
				List<ProchainsDepartsTab> departList = JSONParserUtil.parseNextTrain(nextTrains);
				if (null != departList && departList.size() > 0) {
					for (ProchainsDepartsTab departTab : departList) {
						ProchainsDepartsTabHome departDAO = new ProchainsDepartsTabHome();
						departDAO .attachDirty(departTab);
					}    						
				} else {
					//log.debug("there is no next train of : " + area.getIdArea());
				}
			System.out.println(nextTrains);
			
		} finally {
			HibernateUtil.shutdown();
		}
	}
	
}
