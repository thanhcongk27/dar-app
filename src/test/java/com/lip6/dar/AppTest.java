package com.lip6.dar;

import java.util.Iterator;
import java.util.List;

import com.lip6.dar.api.NavitiaAPI;
import com.lip6.dar.hibernate.LinesTab;
import com.lip6.dar.hibernate.LinesTabHome;
import com.lip6.dar.util.JSONParserUtil;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	
    		
    		NavitiaAPI api = new NavitiaAPI();
    		String json_lines = api.getLinesByMode("Train");
    		List<LinesTab> lines = JSONParserUtil.parseLine(json_lines, "Train");
    		// insert to database
    		if (null != lines & lines.size() > 0) {
    			LinesTabHome lineDAO = new LinesTabHome();
    			Iterator<LinesTab> i = lines.iterator();
    			while (i.hasNext()) {
    				LinesTab line = (LinesTab) i.next();
    				lineDAO.attachDirty(line);
    			}
    		}
    		//log.info(lines);
    	
        assertTrue( true );
    }
}
